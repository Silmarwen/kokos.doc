$(document).ready(function () {
    var mainMenu = $('.js-main-menu'),
        H = $('html');

    setTimeout(function () {
        H.addClass('pageload');
    }, 1500);

    $('.js-hmb').on('click.menu', function (e) {
        e.preventDefault();
        $(this).toggleClass('is-open');
        mainMenu.toggleClass('is-open');
    });

    $('.main-menu .is-parent > a').on('click.menu', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('is-active');
    });
    $('.main-menu .is-parent').on('mouseleave.menu', function () {
        $(this).removeClass('is-active');
    });

    // faq
    $('.js-toggle').on('click.toggle', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('is-open');
    });

    (function ($) {
        var bar = $('.progress-bar'),
            pHtml = '<div class=\'progress-bar__t\'/>';

        bar.each(function () {
            var that = $(this),
                p;

            p = that.data('progress');
            that.pt = $(pHtml).css({'max-width': p + '%'});
            that.append(that.pt);
        })
    }(jQuery));

    if (window.matchMedia('(max-width: 768px)').matches) {
        $('.portfolio-anons__list').bxSlider({
            maxSlides: 1,
            minSlides:1,
            slideWidth: 640,
            controls: false,
            pager: false
        })
    }

    siteConstuctor = $('.sc').sc();

    // progress-bar
    $('.round-progress-bar').each(function () {
        setProgress(this);
    });


    // test
    var i = 0;
    var reverse = false;

    setInterval(function () {
        if (reverse) {
            i-= 10;
        } else {
            i+= 10;
        }

        if (i == 0)
            reverse = false;
        if (i == 100)
            reverse = true;

        setProgress('#bar-01', i);
    }, 1000);

});