(function ($) {
    'use strict';

    var sc,
        options = {
            navigation: '.sc__navigate-step',
            step: '.sc__step',
            pager: '.sc-pageof',
            nextBtn: $('.sc-next'),
        },
        // public methods
        methods = {
            init: function () {
                sc = this;
                sc.currentStep = 0;

                sc.options = options;
                sc.navigation = $(sc.options.navigation);
                sc.steps = $(sc.options.step);
                sc.pager = $(sc.options.pager);

                sc.navigation.on('click.sc', helpers.gotoStep);

                // disabled button
                $('.is-disabled ').on('click.sc', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                });

                sc.options.nextBtn.on('click.sc', function () {
                    if (!$(this).is("[disabled]")) {
                        methods.nextStep();
                    }
                });

                return this;


            },
            nextStep: function () {
                helpers.nextStep();
            },
            prevStep: function () {
                helpers.prevStep();
            }
        },
        // private methods
        helpers = {
            prevStep: function () {
                // TODO:  добавить защиту от ухода в минус
                sc.currentStep--;
                helpers.showStep();
            },
            nextStep: function () {
                // TODO:  добавить защиту от переполнения
                sc.currentStep++;
                helpers.showStep();
            },
            gotoStep: function () {
                sc.currentStep = $(this).index();
                helpers.showStep();
            },
            navigationMove: function () {

            },
            showStep: function () {
                sc.pager.html(sc.currentStep + 1 + '/3');
                sc.navigation.eq(sc.currentStep).addClass('is-active').siblings().removeClass('is-active');
                sc.steps.eq(sc.currentStep).addClass('is-active').siblings().removeClass('is-active');
                $('[data-masonry]').masonry();
            }
        };

    $.fn.sc = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.sNote');
        }
    };
}(jQuery, window));


// helpers
function setProgress(selector, value) {
    var self = $(selector);
    var $circle = self.find('.bar'),
        r = $circle.attr('r'),
        p,
        pct,
        c = Math.PI * (r * 2);

    p = (typeof value != 'undefined') ? value : self.data('progress');
    pct = ((100 - p) / 100) * c;
    $circle.css({strokeDashoffset: pct});
    self.attr('data-progress', p);

    if (p < 25) {
        self.attr('data-theme', 'red')
    }
    else if (p < 50) {
        self.attr('data-theme', 'yellow')
    }
    else {
        self.attr('data-theme', 'green')
    }
}